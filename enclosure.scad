include <coordinates.scad>

// Edit for testing purposes
screwJoins = [
    [0,1],
    [1,2],
    [2,3],
    [3,6],
    [5,6],
    [4,5],
    [0,4],
    [8,7],
    [4,7],
    [8,6]
];

module screwFixation(z=2){
    // creates all standoffs at the specified height

    // Edit holes to render for testing purposes
    for (i=[0:8]) {
        p = screwVertex(i);
        standoff(p[0], p[1], z);
    }
}

module standoff(x, y, z) {
    // Standoffs for the motherboard

    standoffHeight = 8 + UPPER_LEVEL_Z;
    standoffRad = 6 / 2;
    
    translate([x, y, z]) {
        difference(){
            cylinder(r=standoffRad, h=standoffHeight);
            cylinder(r=HOLE_RADIUS, h=standoffHeight);
        }
    }
}

module joinVertex(vertex1, vertex2, h=2, w=10){
    // Connects 2 vertices with a rounded rectangle of width 'w'

    translate(vertex1)
        cylinder(r=w/2, h=h);
    translate(vertex2)
        cylinder(r=w/2, h=h);
    x = vertex1[0] - vertex2[0];
    y = vertex1[1] - vertex2[1];
    d = sqrt( (x*x) + (y*y) );
    flip = x<0 || y<0 ? 180 : 0;
    rot = atan(y/x);
    translate(vertex2) {
        rotate([0, 0, rot + flip]) {
            translate([0, -w/2, 0]) {
                cube(size=[d, w, h]);
            }
        }
    }
    
}

module ethernetJack(x=0, y=0, z=0){
    // makes the ethernet jack
    // Specific bottom for this motherboard

    CAP_INNER_HEIGHT = 12.3;
    CAP_OUTER_HEIGHT = 7.9;
    PLUG_OUTER_HEIGHT = 5.7;
    PLUG_MIDDLE_HEIGHT = 3.6;
    PLUG_INNER_HEIGHT = 1;

    PLUG_OUTER_WIDTH = 11.9;
    PLUG_MIDDLE_WIDTH = 6.2;
    PLUG_INNER_WIDTH = 4.06;
    PLUG_DEPTH = 2.15;
    WALL_HEIGHT = 7.5;
    WALL_WIDTH = 2.53;
    WALL_DEPTH = 9.3;

    segmentWall = WALL_WIDTH;
    segmentOuter = (PLUG_OUTER_WIDTH - PLUG_MIDDLE_WIDTH) / 2;
    segmentMiddle = (PLUG_MIDDLE_WIDTH - PLUG_INNER_WIDTH) / 2;
    segmentInner = PLUG_INNER_WIDTH;

    // the whole jack was calibrated with standoffs translated to Z=2
    // so we translate it down 2
    // The offset is also measured from the scew to the left upper corner

    // compensate for ethernet metal top wall (where the position is measured
    // starting further than the plastic part
    translate([x, y + ETHERNET_Y_COMPENSATE, z - 2]){
        rotate([0,0,-90]){
            cube(size=[PLUG_OUTER_WIDTH + segmentWall, WALL_DEPTH, PLUG_INNER_HEIGHT]);
            cube(size=[segmentWall, WALL_DEPTH, WALL_HEIGHT]);
            translate([WALL_WIDTH + PLUG_OUTER_WIDTH, 0, 0])
                cube(size=[segmentWall, WALL_DEPTH, WALL_HEIGHT]);
            
            translate([WALL_WIDTH, 0, 0]) 
                cube(size=[segmentOuter, WALL_DEPTH, PLUG_OUTER_HEIGHT]);

            translate([WALL_WIDTH + segmentOuter, 0, 0])
                cube(size=[segmentMiddle, PLUG_DEPTH, PLUG_MIDDLE_HEIGHT]);

            translate([WALL_WIDTH + segmentOuter + segmentMiddle, 0, 0])
                cube(size=[segmentInner, PLUG_DEPTH, PLUG_INNER_HEIGHT]);
            
            translate([WALL_WIDTH + segmentOuter + segmentMiddle + segmentInner, 0, 0])
                cube(size=[segmentMiddle, PLUG_DEPTH, PLUG_MIDDLE_HEIGHT]);

            translate([WALL_WIDTH + segmentOuter + 2*segmentMiddle + segmentInner, 0, 0])
                cube(size=[segmentOuter, WALL_DEPTH, PLUG_OUTER_HEIGHT]);
        }
    }

}

module faceplate(z=2){
    // holes for the connectors
    // uses FACEPLATE_TOLERANCE to get better clearings and account for alignment errors

    // jack
    JACK_RADIUS = POWER_JACK_HEIGHT/2 + FACEPLATE_TOLERANCE;

    translate([-1, 0, z + FACEPLATE_TOLERANCE]){
        // jack
        translate(POWER_JACK_POS)
            translate([0, -POWER_JACK_HEIGHT/2, -POWER_JACK_HEIGHT/2])
                rotate([0, 90, 0])
                    cylinder(r=JACK_RADIUS, h=10);

        // vga
        translate(VGA_POS)
            translate([0, -VGA_WIDTH, -VGA_HEIGHT])
                cube(size=[
                    10,
                    VGA_WIDTH + 2*FACEPLATE_TOLERANCE,
                    VGA_HEIGHT + 2*FACEPLATE_TOLERANCE
                ]);

        // hdmi
        translate(HDMI_POS)
                translate([0, -HDMI_WIDTH, -HDMI_HEIGHT])
                    cube(size=[
                        10,
                        HDMI_WIDTH + 2*FACEPLATE_TOLERANCE,
                        HDMI_HEIGHT + 2*FACEPLATE_TOLERANCE
                    ]);
        
        // usb
        translate(USB_POS)
                translate([0, -USB_WIDTH, -USB_HEIGHT])
                    cube(size=[
                        10,
                        USB_WIDTH + 2*FACEPLATE_TOLERANCE,
                        USB_HEIGHT + 2*FACEPLATE_TOLERANCE
                    ]);
        
        // ethernet
        translate(ETHERNET_HOLE_POS)
                translate([0, -ETHERNET_WIDTH, -ETHERNET_HEIGHT])
                    cube(size=[
                        10,
                        ETHERNET_WIDTH,
                        ETHERNET_HEIGHT +2*FACEPLATE_TOLERANCE
                    ]);
        
    }
}

module fanStandoff(x, y, z, h){
    // Standoff for the fan
    // Must be used with the fanScrewHoles in the difference, or else the holes may
    // be plugged in unions with other objects

    // TODO: Merge with standoff function as they are quite similar

    standoffRad = 6 / 2;
    
    translate([x, y, z]) {
        difference(){
            cylinder(r=standoffRad, h=h);
            cylinder(r=HOLE_RADIUS, h=h);
        }
    }
}

module fanScrewHoles(x, y, z){
    // Fan mount holes to be used in 'difference'
    // Since the fan Standoffs are shorter than the screws

    translate([x, y, z - 4]) {
            cylinder(r=HOLE_RADIUS, h=4);
    }
}

module fanMount(){
    // Places all fan standoffs

    for (i=[0:len(FAN_MOUNT_POINTS) - 1]) {
        fanStandoff(
            fanVertex(i)[0],
            fanVertex(i)[1],
            2,
            FAN_MOUNT_HEIGHT[i]);
    }
}

module bottomChassis(){
    CHASSIS_WIDTH = 220;
    CHASSIS_DEPTH = 170;
    CHASSIS_EXTRA_ROOM = 20;
    CHASSIS_THICKNESS = 3;
    MB_HEIGHT = 25 + CHASSIS_THICKNESS;
    STAND_RADIUS = CHASSIS_EXTRA_ROOM / 4;

    INSERT_RADIUS = 2.6;
    INSERT_HEIGHT = 7;

    FAN_INTAKE_RADIUS = 20;

    translate([FACEPLATE_X, POWER_JACK_POS[1] + CHASSIS_THICKNESS + (CHASSIS_EXTRA_ROOM / 2) ,-CHASSIS_THICKNESS]){

        // Bottom
        difference(){
            hull() {
                for (x=[ STAND_RADIUS, CHASSIS_WIDTH - STAND_RADIUS ]) {
                    for(y=[ -STAND_RADIUS, -CHASSIS_DEPTH + STAND_RADIUS ]){
                        translate([x,y])
                            cylinder(r=STAND_RADIUS, h=CHASSIS_THICKNESS, center=false);
                    }
                }
            }

            // Fan Intake
            translate([55 + FAN_INTAKE_RADIUS, -105 -FAN_INTAKE_RADIUS])
                cylinder(r=FAN_INTAKE_RADIUS, h=CHASSIS_THICKNESS*2, center=true);

        }

        difference(){
            union(){
                // Stands
                for (x=[ STAND_RADIUS, CHASSIS_WIDTH - STAND_RADIUS ]) {
                    for(y=[ -STAND_RADIUS, -CHASSIS_DEPTH + STAND_RADIUS ]){
                        translate([x,y])
                            cylinder(r=STAND_RADIUS, h=MB_HEIGHT, center=false);
                    }
                }

                // Walls
                //
                // Only 3, no wall on the faceplate
                translate([STAND_RADIUS, -CHASSIS_THICKNESS]){
                    cube(size=[CHASSIS_WIDTH - 2* STAND_RADIUS, CHASSIS_THICKNESS, MB_HEIGHT], center=false);

                    translate([0,-CHASSIS_DEPTH + CHASSIS_THICKNESS])
                        cube(size=[CHASSIS_WIDTH - 2* STAND_RADIUS, CHASSIS_THICKNESS, MB_HEIGHT], center=false);
                }

                translate([CHASSIS_WIDTH - CHASSIS_THICKNESS, -CHASSIS_DEPTH + STAND_RADIUS, 0]) {
                    cube([CHASSIS_THICKNESS, CHASSIS_DEPTH - 2* STAND_RADIUS, MB_HEIGHT]);
                }

                // Faceplate reinforce
                // triangle plus 0.7 rectangle (full chassis thickness interferes with the faceplate holes)
                translate([0, -STAND_RADIUS, MB_HEIGHT - 1.7 * CHASSIS_THICKNESS])
                rotate([90, 0, 0]) {
                    linear_extrude(CHASSIS_DEPTH - 2 * STAND_RADIUS){
                        polygon(points=[
                            [0,0],
                            [CHASSIS_THICKNESS, CHASSIS_THICKNESS], 
                            [CHASSIS_THICKNESS, 1.7 * CHASSIS_THICKNESS],
                            [0, 1.7 * CHASSIS_THICKNESS],
                            [0, CHASSIS_THICKNESS]
                        ]);
                    }
                }
                
                
            }

            // Insert holes
            for (x=[ STAND_RADIUS, CHASSIS_WIDTH - STAND_RADIUS ]) {
                for(y=[ -STAND_RADIUS, -CHASSIS_DEPTH + STAND_RADIUS ]){
                    translate([x,y, MB_HEIGHT - INSERT_HEIGHT]){
                            cylinder(r=INSERT_RADIUS, h=INSERT_HEIGHT, center=false);
                    }
                }
            }
        }
    }
}

module skeleton(){
    ethernetJack(ETHERNET_POS[0], ETHERNET_POS[1], 2 + UPPER_LEVEL_Z);

    // 0.01 = "hack" so that prusaslicer slices correctly
    // Also create it again to join the real jack to the faceplate
    ethernetJack(FACEPLATE_X + 0.01, ETHERNET_POS[1], 2 + UPPER_LEVEL_Z);

    screwFixation();
    fanMount();

    difference(){
        union(){
            // skeleton joins
            for (i=screwJoins) {
                joinVertex(screwVertex(i[0]), screwVertex(i[1]));
            }
            // fan mount skeleton joins
            joinVertex(fanVertex(2), screwVertex(3), w=7, h=2);
        }
        // fan mount holes
        for (i=[0:len(FAN_MOUNT_POINTS) - 1]) {
            fanScrewHoles(
                fanVertex(i)[0],
                fanVertex(i)[1],
                2 + FAN_MOUNT_HEIGHT[i]);
        }
    }

    // ethernet reinforce join
    translate([1, ETHERNET_POS[1] - 16])
        cube(size=[10, 18, UPPER_LEVEL_Z]);

    // Faceplate
    PLATE_WIDTH = 160;

    difference(){
        translate([FACEPLATE_X, -PLATE_WIDTH + POWER_JACK_POS[1] + 8, 0])
            cube([1, PLATE_WIDTH, 25]);
        faceplate(2 + UPPER_LEVEL_Z);
    }
    translate([FACEPLATE_X, -100])
        cube([15, 50, 2]);

}


skeleton();

bottomChassis();