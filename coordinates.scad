
UPPER_LEVEL_Z = 6;

// #####################################################################################
//      Holes
// #####################################################################################

HOLE_CONVERSION_FACTOR = 10.21 / 100;

PIXEL_SCREW_POSITIONS = [
    [291, 704],
    [282, 1181],
    [194, 1935],
    [604, 1935],
    [1230, 868],
    [1230, 1526],
    [1235, 1935],
    [2042, 1460],
    [1970, 1817]
];

SCREW_OFFSET = [
    194*HOLE_CONVERSION_FACTOR - 10,
    704*HOLE_CONVERSION_FACTOR - 10
];

function screwVertex(idx, z=0) = [
    PIXEL_SCREW_POSITIONS[idx][0]*HOLE_CONVERSION_FACTOR - SCREW_OFFSET[0],
    - (PIXEL_SCREW_POSITIONS[idx][1]*HOLE_CONVERSION_FACTOR - SCREW_OFFSET[1]),
    z
];

HOLE_RADIUS = 2.1 / 2;

// #####################################################################################
//      Ethernet
// #####################################################################################

// Offset from Screw idx=0
// -1.32 
ETHERNET_0_OFFSET = [-18.15, 4.65];

// Top of plug is further in Y than the bottom, this is the amount
ETHERNET_Y_COMPENSATE = 1.32;
ETHERNET_METAL_CONNECTOR_WIDTH = 1.06;

ETHERNET_POS = [
    screwVertex(0)[0] + ETHERNET_0_OFFSET[0],
    screwVertex(0)[1] - ETHERNET_0_OFFSET[1],
    -2
];

// #####################################################################################
//      Face Plate
// #####################################################################################

/*
    Faceplate has 2 images with different conversion factors:
        Image1 is top-down
        Image2 is front of the connectors
    
    Power Jack position is derived from Ethernet jack
    All other connectors are derived from the Power jack
    Z offsets are measured from the desk with standoffs that have extra height 2mm
    
    The plate X start coordinate is derived from the VGA port, which is the most
    protuding
*/

FACEPLATE_CONVERSION_FACTOR_1 = 100 / 1631.1;
FACEPLATE_CONVERSION_FACTOR_2 = 50 / 1124.4;

// Image 1 offsets:
POWER_JACK_Y_OFFSET_PX = 254;
POWER_JACK_WIDTH = 114 * FACEPLATE_CONVERSION_FACTOR_1;

VGA_Y_OFFSET_PX = 658;
VGA_WIDTH = 260 * FACEPLATE_CONVERSION_FACTOR_1;

HDMI_Y_OFFSET_PX = 1073;
HDMI_WIDTH = 247 * FACEPLATE_CONVERSION_FACTOR_1;

USB_Y_OFFSET_PX = 1420;
USB_WIDTH = 224 * FACEPLATE_CONVERSION_FACTOR_1;

// Image2 offsets
POWER_JACK_Z_OFFSET_PX = 255;
POWER_JACK_HEIGHT = 118 * FACEPLATE_CONVERSION_FACTOR_2;

VGA_Z_OFFSET_PX = 297;
VGA_HEIGHT = 180 * FACEPLATE_CONVERSION_FACTOR_2;

HDMI_Z_OFFSET_PX = 298;
HDMI_HEIGHT = 132 * FACEPLATE_CONVERSION_FACTOR_2;

USB_Z_OFFSET_PX = 305;
USB_HEIGHT = 129 * FACEPLATE_CONVERSION_FACTOR_2;

ETHERNET_HEIGHT = 313 * FACEPLATE_CONVERSION_FACTOR_2;

//

ETHERNET_WIDTH = 11.9 ;//+ 2.53 * 2;

FACEPLATE_X = ETHERNET_POS[0] // Start on Ethernet
                - 27*FACEPLATE_CONVERSION_FACTOR_1 // get to the jack depth
                ;//- 1; // gross compensate for picture warping making VGA look like =depth

FACEPLATE_TOLERANCE = 0.3;

POWER_JACK_POS = [
    FACEPLATE_X,
    POWER_JACK_Y_OFFSET_PX*FACEPLATE_CONVERSION_FACTOR_1 + ETHERNET_POS[1],
    POWER_JACK_Z_OFFSET_PX*FACEPLATE_CONVERSION_FACTOR_2 -2
];

ETHERNET_HOLE_POS = [
    FACEPLATE_X,
    ETHERNET_POS[1] + ETHERNET_Y_COMPENSATE - 2.53,
    ETHERNET_HEIGHT -2
];

VGA_POS = [
    FACEPLATE_X,
    POWER_JACK_POS[1] - VGA_Y_OFFSET_PX*FACEPLATE_CONVERSION_FACTOR_1,
    VGA_Z_OFFSET_PX*FACEPLATE_CONVERSION_FACTOR_2 -2
];

HDMI_POS = [
    FACEPLATE_X,
    POWER_JACK_POS[1] - HDMI_Y_OFFSET_PX*FACEPLATE_CONVERSION_FACTOR_1,
    HDMI_Z_OFFSET_PX*FACEPLATE_CONVERSION_FACTOR_2 -2
];

// 0.6 adjustment for V2 after checking V1
USB_POS = [
    FACEPLATE_X,
    POWER_JACK_POS[1] - USB_Y_OFFSET_PX*FACEPLATE_CONVERSION_FACTOR_1 + 0.6,
    USB_Z_OFFSET_PX*FACEPLATE_CONVERSION_FACTOR_2 -2
];


// #####################################################################################
//      Fan mount
// #####################################################################################

// X, Y are pixels
// Z is mm
FAN_MOUNT_POINTS = [
    [0, 126],
    [205, 490],
    [766, 102]
];
FAN_MOUNT_HEIGHT = [
    2.8,
    2.54,
    2.8
];

FAN_CONVERSION_FACTOR = 80 / 938;

function fanVertex(idx, z=0) = [
    -(FAN_MOUNT_POINTS[idx][0] * FAN_CONVERSION_FACTOR) + screwVertex(5)[0],
    -(FAN_MOUNT_POINTS[idx][1] * FAN_CONVERSION_FACTOR) + screwVertex(5)[1],
    z
];