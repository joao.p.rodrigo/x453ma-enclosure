INSERT_RADIUS = [2.0, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6];

// After the tests, radius of 2.6 was the most adequate
INSERT_HEIGHT = 7;
STAND_RADIUS = 5;

EXTRA_HEIGHT = 3;

translate([0, -5])
cube(size=[15 * (len(INSERT_RADIUS) - 1), 10, EXTRA_HEIGHT], center=false);

for (i=[0:len(INSERT_RADIUS) - 1]) {
    translate([i*15, 0, 0]){
        difference(){
            cylinder(r=STAND_RADIUS, h=INSERT_HEIGHT + EXTRA_HEIGHT, center=false);
            translate([0,0,EXTRA_HEIGHT])
                cylinder(r=INSERT_RADIUS[i], h=INSERT_HEIGHT, center=false);
        }

    }
}
